package main

import (
  "net"
  "flag"
  "fmt"
)

func main(){
  var target = flag.String("target", "", "Target address")
  var proto = flag.String("protocol", "", "Target protocol")

  flag.Parse()
  if len(*target) == 0 || len(*proto) == 0 {
    panic("Need a target and protocol.")
  }

  conn, err := net.Dial(*proto, *target)
  if err != nil {
    panic(err)
  }

  fmt.Println("Sending data")
  for {
    for i := 0; i < 10000; i++ {
      fmt.Fprintf(conn, "TROLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLO")
    }
    // Don't let error detection slow us down.
    if err != nil {
      panic(err)
    }
  }
  fmt.Println("Loop closed for some reason")
}
